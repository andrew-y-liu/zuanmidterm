// Andrew Liu
// yliu327
// Rongrong Liu
// rliu39

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdio.h>
#include "ppm_io.h"

/* Check if a string is end with .ppm
 * check command line argument
 */
int check_fargv(char *arg);

/* Check if a string all number
 * or start with a minus sign
 * return 0 everything is fine. else 1
 */
int check_intargv(char * arg);

/* Check if a string is a float
 * return 0 everything is fine. else 1
 */
int check_floatargv(char *arg);

/* This function frees a image
 */
void free_image(Image *im);

/* Create new image after exposure processing
 * taking in origninal image and exposure value
 */
Image *exposure(const Image *orig, float expo);

/* Takes in original color and exposure exponent
 * Return final exposed value
 * Cap at 255
 */
int expo_val(int color, float expo);

/* This function is for alpha blend
 * 
 */
Image *ablend(const Image *im1, const Image *im2, float alph);

/* Update the pixel value when both row and column of resulting image
 * are from the same image
 */
void pixel_same(const Image *big, const Image *small, Image *new, float alph);

/* Update the pixel value when both row and column of resulting image
 * are based on different image
 */
void pixel_diff(const Image *row_b, const Image *col_b, Image *new, float alph);

/* blend two pixel
 * New pixel updated via pointer
 */
void ablend_p(const Pixel px1, const Pixel px2, Pixel *final, float alph);


/* 
 * Zooms in an image to 2*2 of its original scale
 */
Image *zoom_in(const Image *orig);


/* 
 * Zooms out an image to 0.5*0.5 of its original scale
 */ 
Image *zoom_out(const Image *orig);

Image *pointilism(const Image *orig);

int max(int first, int second);

int min(int first, int second);

int within_circle(int center_row, int center_col, int this_row, int this_col, int radius);

Image *swirl(const Image *orig, int cx, int cy, int strength);

Image *blur(const Image *orig, double sigma);

double * gaussian (double sigma, int n);

Pixel * convolve(const Image * orig, int row, int col, int total_row, int total_col, double * gaussian_matrix, int n);

double sq(double a);

#endif

