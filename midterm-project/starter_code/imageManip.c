// Andrew Liu
// yliu327
// Rongrong Liu
// rliu39



#include <stdio.h>
#include <assert.h>
#include "imageManip.h"
#include "ppm_io.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#define PI acos(-1.0) //define the value of pi

/* Check if a string is end with .ppm
 * check command line argument
 * return 0 if end with .ppm else return 1
 */
int check_fargv(char *arg) {
  char *check = &(arg[strlen(arg) - 4]); //point to the last beginning of the last four char
  char *ending = ".ppm";
  if (strcmp(check, ending) == 0) {
    return 0;
  }
  else {
    return 1;
  }
}

/* Check if a string is integer expression
 * return 0 everything is fine. else 1
 */
int check_intargv(char * arg) {
  // first char can be minus
  if (arg[0] != 45 && (arg[0] < '0' || arg[0] >'9')) {
    return 1;
  }
  // check is other are digits
  for (int i = 1; i < (int)strlen(arg); i++) {
    if (arg[i] >= '0' && arg[i] <= '9') {
      continue;
    }
    else {
      return 1;
    }
  }
  return 0;
}

/* Check if a string is a float expression
 * return 0 everything is fine. else 1
 */
int check_floatargv(char *arg) {
  int dec_flag = 0; //mark decimal point
  //check first, can be minus sign
  if ((arg[0] != '-' && arg[0] != '.') && (arg[0] < '0' || arg[0] >'9')) {
    return 1;
  }
  //check there is only one decimal point
  for (int i = 0; i < (int)strlen(arg); i++) {
    //first occurance
    if (arg[i] == '.' && dec_flag == 0) {
      dec_flag = 1;
      continue;
    }
    //other occurance
    else if (arg[i] == '.') {
      return 1;
    }
  }
  //check digits
  for (int i = 1; i < (int)strlen(arg); i++) {
    if (arg[i] == '.' || (arg[i] >= '0' && arg[i] <='9')) {
      continue;
    }
    else {
      return 1;
    }
  }
  return 0;
}

/* Create new image after exposure processing
 * taking in origninal image and exposure value
 */
Image *exposure(const Image *orig, float expo) {
  Image *new = malloc(sizeof(Image));

  // record basic size info
  new->cols = orig->cols;
  new->rows = orig->rows;
  new->data = malloc(sizeof(Pixel) * new->rows * new->cols);

  // use update exposure
  for (int r = 0; r < new->rows; r++) {
    for (int c = 0; c < new->cols; c++) {
      new->data[r * new->cols + c].r = expo_val(orig->data[r * new->cols + c].r, expo);
      new->data[r * new->cols + c].g = expo_val(orig->data[r * new->cols + c].g, expo);
      new->data[r * new->cols + c].b = expo_val(orig->data[r * new->cols + c].b, expo);
    }
  }

  return new;
}

/* Takes in original color and exposure exponent
 * Return final exposed value
 * Cap at 255
 */
int expo_val(int color, float expo) {
  int new = color * pow(2, expo);
  if (new > 255) {
    return 255;
  }
  else {
    return new;
  }
}

/* This function is for alpha blend
 * taking in 2 images and alpha blend values
 */
Image *ablend(const Image *im1, const Image *im2, float alph) {
  Image *new = malloc(sizeof(Image)); // create new image
  int r_flag = 1; // flags indicating base of row_num
  int c_flag = 1; // flags indicating base of col_num

  //initialize size data
  //assign columns
  if (im1->cols > im2->cols) {
    new->cols = im1->cols;
  }
  else {
    new->cols = im2->cols;
    c_flag = 2;
  }
  //assign rows
  if (im1->rows > im2->rows) {
    new->rows = im1->rows;
  }
  else {
    new->rows = im2->rows;
    r_flag = 2;
  }
  new->data = malloc(sizeof(Pixel) * new->cols * new->cols);
  // row and col depends on the same image
  if (r_flag == c_flag) {
    if (r_flag == 1) {
      pixel_same(im1, im2, new, alph);
    }
    else {
      pixel_same(im2, im1, new, 1 - alph);
    }
  }
  // row and col depends on different image
  else {
    if (r_flag == 1) {
      pixel_diff(im1, im2, new, alph);
    }
    else {
      pixel_diff(im2, im1, new, 1 - alph);
    }
  }
  return new;
}

/* Update the pixel value when both row and column of resulting image
 * are from the same image
 */
void pixel_same(const Image *big, const Image *small, Image *new, float alph) {
  for (int r = 0; r < big->rows; r++) {
    for (int c = 0; c < big->cols; c++) {
      if (r < small->rows) {
        if (c < small->cols) {
          //need blend part
          ablend_p(big->data[r * big->cols + c], small->data[r * small->cols + c], &(new->data[r * new->cols + c]), alph);
        }
        else {
          new->data[r * big->cols + c] = big->data[r * big->cols + c];
        }
      }
      else {
        new->data[r * big->cols + c] = big->data[r * big->cols + c];
      }
    }
  }
}

/* Update the pixel value when both row and column of resulting image
 * are based on different image
 */
void pixel_diff(const Image *row_b, const Image *col_b, Image *new, float alph) {
  for (int r = 0; r < row_b->rows; r++) {
    for (int c = 0; c < col_b->cols; c++) {
      if (r < col_b->rows) {
        if (c < row_b->cols) {
          //top left
          ablend_p(row_b->data[r * row_b->cols + c], col_b->data[r * col_b->cols + c], &(new->data[r * new->cols + c]), alph);
        }
        else {
          //top right
          new->data[r * new->cols + c] = col_b->data[r * col_b->cols + c];
        }
      }
      else {
        if (c < row_b->cols) {
          //bottom left
          new->data[r * new->cols + c] = row_b->data[r * row_b->cols + c];
        }
        else {
          //bottom right
          new->data[r * new->cols + c].r = 0;
          new->data[r * new->cols + c].g = 0;
          new->data[r * new->cols + c].b = 0;
        }
      }
    }
  }
}

/* blend two pixel
 * New pixel updated via pointer
 */
void ablend_p(const Pixel px1, const Pixel px2, Pixel *final, float alph) {
  final->r = alph * px1.r + (1 - alph) * px2.r;
  final->g = alph * px1.g + (1 - alph) * px2.g;
  final->b = alph * px1.b + (1 - alph) * px2.b;
}

/* free the image
 */
void free_image(Image *im) {
  free(im->data);
  free(im);
}

/* zoom in an image
 */
Image *zoom_in(const Image *orig){
  Image *new = malloc(sizeof(Image));
  // record basic size info
  new->cols = orig ->cols * 2;
  new->rows = orig ->rows * 2;
  new->data = malloc(sizeof(Pixel) * new->rows * new->cols);

  for (int i = 0; i < new->rows; i++) {
    for (int j = 0; j < new->cols; j++) {
      //calculate corresponding row and column of original image
      //from given row and column in new image
      int row_in_orig = i / 2;
      int col_in_orig = j / 2;
      //assign rgb value to each pixel
      new->data[i * new->cols + j].r = orig->data[row_in_orig * orig->cols + col_in_orig].r;
      new->data[i * new->cols + j].g = orig->data[row_in_orig * orig->cols + col_in_orig].g;
      new->data[i * new->cols + j].b = orig->data[row_in_orig * orig->cols + col_in_orig].b;
    } 
  }
    return new;
}


/* The function zoom out a function
 */
Image *zoom_out(const Image *orig) {
  Image *new = malloc(sizeof(Image));
  // record basic size info
  new ->cols = orig->cols / 2;
  new->rows = orig->rows / 2;
  new->data = malloc(sizeof(Pixel) * new->rows * new->cols);
  for (int i = 0; i < new->rows; i++) {
    for (int j = 0; j < new->cols; j++) {
    //calculate the four index of 2*2 pixel matrix in original image 
      int index1 = i * 2 * orig ->cols + j * 2;
      int index2 = i * 2 * orig ->cols + j * 2 + 1;
      int index3 = (i * 2 + 1) * orig ->cols + j * 2;
      int index4 = (i * 2 + 1) * orig ->cols + j * 2 + 1;
      //assign rgb value to each pixel
      new->data[i * new->cols + j].r = (int)(((orig->data[index1].r + orig->data[index2].r +
        orig->data[index3].r + orig->data[index4].r)) / 4);
      new->data[i * new->cols + j].g = (int)(((orig->data[index1].g + orig->data[index2].g +
        orig->data[index3].g + orig->data[index4].g)) / 4);
      new->data[i * new->cols + j].b = (int)(((orig->data[index1].b + orig->data[index2].b +
        orig->data[index3].b + orig->data[index4].b)) / 4);
    } 
  }
  return new;
}

/* The function creates the pointlism of an image
 */
Image *pointilism(const Image *orig){
  Image *new = malloc(sizeof(Image));
  // record basic size info
  new->cols = orig->cols;
  new->rows = orig->rows;
  new->data = malloc(sizeof(Pixel) * new->rows * new->cols);
    
  //assign the color of each pixel from old image to new image
  for (int i = 0; i < new->rows; i++){
    for (int j = 0; j < new->cols; j++){
      new->data[i * new->cols + j].r = orig ->data[i * new->cols + j].r;
      new->data[i * new->cols + j].g = orig ->data[i * new->cols + j].g;
      new->data[i * new->cols + j].b = orig ->data[i * new->cols + j].b;
    }
  }

  //calculate number of pixels in the picture
  int numPixels = new->rows * new->cols;
  //calculate number of points to be pointilized in the picture
  int numPoints = numPixels * 0.03;

  for (int count = 0; count < numPoints; count++) {
    //generate a random row, a random column, and a random radius between 1 and 5
    int rand_col = rand() % new->cols;
    int rand_row = rand() % new->rows;
    int rand_radius = (rand() % 5) + 1;
    //loop through each pixel in image, change its color to color 
    //of center of the circle if it is within the circle
    for (int i = max(rand_row - rand_radius, 0); i < min(rand_row + rand_radius + 1, new->rows); i++) {
      for (int j = max(rand_col - rand_radius, 0); j < min(rand_col + rand_radius + 1, new->cols); j++) {
        if (within_circle(rand_row, rand_col, i, j, rand_radius) == 1) {
          new->data[i * new->cols + j].r = new->data[rand_row * new->cols + rand_col].r;
          new->data[i * new->cols + j].g = new->data[rand_row * new->cols + rand_col].g;
          new->data[i * new->cols + j].b = new->data[rand_row * new->cols + rand_col].b;
        }
      }
    }
  }
  return new;
}

/* Find the bigger integers of 2
 */
int max(int first, int second) {
  if (first > second) {
    return first;
  }
  return second;
}

/* Find the smaller integers of 2
 */
int min(int first, int second) {
  if (first < second) {
    return first;
  }
  return second;
}

/* Takes in row & column of a pixel, center of a circle, and radius of the circle
 * Returns 1 if the pixel is within the circle, 0 if it is not.
 */
int within_circle(int center_row, int center_col, int this_row, int this_col, int radius) {
  int row_diff = this_row - center_row;
  int col_diff = this_col - center_col;
  if(pow(row_diff, 2) + pow(col_diff, 2)  <= pow(radius, 2)) {
    return 1;
  }
return 0;
}

/* The function swirl an image
 * param: image, center coordinate, swirl strength
 */
Image *swirl(const Image *orig, int cx, int cy, int strength) {
  Image *new = malloc(sizeof(Image));
  // record basic size info
  new->cols = orig->cols;
  new->rows = orig->rows;
  new->data = calloc(new->rows * new->cols, sizeof(Pixel));
  //loop through each index in new image
  for (int x = 0; x < new->cols; x++){
    for (int y = 0; y < new->rows; y++){
      //calculate distance
      double dx = x - cx;
      double dy = y - cy;
      double distance = sqrt(dx * dx + dy * dy);
      //calculate the angle alpha
      double alpha = distance / strength; 
      //find corresponding row and column of pixel in original picture
      int orig_x = (int)(dx * cos(alpha) - dy * sin(alpha) + cx);
      int orig_y = (int)(dx * sin(alpha) + dy * cos(alpha) + cy);
      //assign color to pixel at location (x, y) in the swirled image
      if (orig_x >= 0 && orig_x < orig->cols && orig_y >= 0 && orig_y < orig->rows) {
        new->data[y * new->cols + x].r = orig->data[orig_y * orig->cols + orig_x].r;
        new->data[y * new->cols + x].g = orig->data[orig_y * orig->cols + orig_x].g;
        new->data[y * new->cols + x].b = orig->data[orig_y * orig->cols + orig_x].b;
      }
    }
  }
  return new;
}

/* Guassian blurring an image
 * takes image and sigma to Guassian distribution
 */
Image *blur(const Image *orig, double sigma) {
  Image *new = malloc(sizeof(Image));
  // record basic size info
  new->cols = orig->cols;
  new->rows = orig->rows;
  new->data = malloc(sizeof(Pixel) * new->rows * new->cols);
  int n = (int)(sigma * 10);
  // N should be big enough to be at least 10*sigma positions wide
  // and should always be an odd number
  if(n % 2 == 0){
    n++;
  }
  //calculate the gaussian matrix
  double *gaussian_mat = gaussian(sigma, n);
  //apply filter to each pixel and blur the image
  for (int i = 0; i < new->rows; i++){
    for (int j = 0; j < new ->cols; j++){
      Pixel * pix = convolve(orig, i, j, new->rows, new->cols, gaussian_mat, n);
      new->data[i * new->cols + j] = * pix;
      free(pix);
    }
  }
  free (gaussian_mat);
  return new;
}

/* The funciton creates the gaussian kernel
 * per size request
 */
double * gaussian (double sigma, int n){
  double * matrix = malloc(sizeof(double) * n * n);
  int half = n / 2;
  for (int i = 0; i < n; i++){
    for (int j = 0; j < n; j++){
      // calculate gaussian value for each element in the matrix
      double dx = j - half;
      double dy = i - half;
      double g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
      //assign gaussian value
      matrix[i * n + j] = g;
    }
  }
  return matrix;
}

/* this function returns a pointer to a new pixel whose is the weighted average of pixels
 * around this pixel
 */
Pixel * convolve(const Image * orig, int row, int col, int total_row, int total_col, double * gaussian_matrix, int n){
  int half = n / 2;
  //allocate memory for result pixel
  Pixel * pix = malloc(sizeof(Pixel));
  //initialize weight, red, green, blue value
  double total_weight = 0.0;
  double red = 0;
  double green = 0;
  double blue = 0;
  //loop through each element within the gaussian matrix, near center position
  //not that max and min functions prevents the function from going out of bound
  for (int i = max(0, row - half); i < min(total_row, row + half + 1); i++) {
    for (int j = max(0, col - half); j < min(total_col, col + half + 1); j++) {
      int r = half + (row - i);
      int c = half + (col - j);
      total_weight = total_weight + gaussian_matrix[r * n + c];
      red = red + gaussian_matrix[r * n + c] *( (double)(orig->data[i * total_col + j].r));
      green = green + gaussian_matrix[r * n + c] * ((double)(orig->data[i * total_col + j].g));
      blue = blue + gaussian_matrix[r * n + c] * ((double)(orig->data[i * total_col + j].b));
    }
  }
  //assign rgb value to new pixel
  pix->r = (unsigned char)(red / total_weight);
  pix->g = (unsigned char)(green / total_weight);
  pix->b = (unsigned char)(blue / total_weight);
  return pix;
}
  
/* this function squares a double
 */
double sq(double a) {
  double square = a * a;
  return square;
}
