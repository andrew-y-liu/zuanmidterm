// Andrew Liu
// yliu327
// Rongrong Liu
// rliu39


// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>
#include <string.h>
#include "imageManip.h"




/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  rewind(fp);
  //check format information of content
  char format[3];
  fscanf(fp, " %s", format);

  //skip comment lines
  char c;
  while (fscanf(fp, " #%c", &c)) {
    while (fscanf(fp, "%c", &c)) {
      if (c == '\n') break;
    }
  }

  //read size and color format information
  int row, col, color;
  fscanf(fp, " %d %d %d", &col, &row, &color);

  //create image structure
  Image *im = malloc(sizeof(Image));
  if (im == NULL) {
    printf("fail to create image pointer\n");
    return NULL;
  }

  // assign size information
  im->rows = row;
  im->cols = col;
  im->data = malloc(sizeof(Pixel) * im->cols * im->rows);

  //get rid of the new line char
  char s[1];
  fread(s, 1, 1, fp);

  int count = fread(im->data, sizeof(Pixel), col * row + 1, fp);
  //read the information and write the color to pixels

  if (count != (row * col)) {
    free_image(im);
    return NULL;
  }
  return im;  //TO DO: replace this stub
}

/* This function checks whether the input format is correct
 * Check image and color format
 */
int check_in(FILE *fp) {
  char format[3];
  char *std = "P6";
  fscanf(fp, " %s", format);
  if (strcmp(format, std) != 0) {
    printf("incorrect image format\n");
    return 1;
  }

  char c;
  while (fscanf(fp, " #%c", &c)) {
    while (fscanf(fp, "%c", &c)) {
      if (c == '\n') break;
    }
  }

  //read size and color format information
  int row, col, color;
  fscanf(fp, " %d %d %d", &row, &col, &color);
  if (color != 255 || row < 0 || col < 0) {
    printf("unsupported color format\n");
    return 1;
  }
  return 0;
}

/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }
  return num_pixels_written;
}
