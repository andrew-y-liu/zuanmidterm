// Andrew Liu
// yliu327
// Rongrong Liu
// rliu39


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"
#include "imageManip.h"


int main(int argc, char *argv[]) {
    
    //check if input and output file names are supplied
    if (argc < 3 || check_fargv(argv[1]) || check_fargv(argv[2])) {
        printf("Failed to supply input file name or output file name, or both\n");
        return 1;
    }
    // check do we have operation
    if (argc < 4) {
        printf("no operation specified.\n");
        return 4;
    }

    //open input file
    FILE *in = fopen(argv[1], "rb");
    //check if the input file is correctly inputed
    if (in == NULL) {
        printf("Specified input file could not be opened.\n");
        return 2;
    }

    //format check
    if (check_in(in)) {
        printf("Specified input file is not a properly-formatted PPM file, or reading input somehow fails\n");
        fclose(in);
        return 3;
    }

    //read the image
    Image *in_ppm = read_ppm(in);
    fclose(in);

    if (in_ppm == NULL) {
      printf("Specified input file is not a properly-formatted PPm file, or reading input somehow fails\n");
      return 3;
    }

    FILE *out = fopen(argv[2], "wb");

    if (out == NULL) {
        printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
        free_image(in_ppm);
        return 7;
    }

    int exit = 0;

    //content checker
    char *expos = "exposure";
    char *abl = "blend";
    char *z_in = "zoom_in";
    char *z_out = "zoom_out";
    char *ptl = "pointilism";
    char *swr = "swirl";
    char *bl = "blur";

    // exposure
    if (strcmp(argv[3], expos) == 0) {
        if (argc != 5) {
            printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
            fclose(out);
            free_image(in_ppm);
            return 5;
        }
        int expo = atof(argv[4]); //FIXME: need error checking for last

        if (expo < -3 || expo > 3 || check_floatargv(argv[4])) {
            fclose(out);
            free_image(in_ppm);
            printf("Arguments for the specified operation were out of range for the given input image, or otherwise senseless\n");
            return 6;
        }

        Image *out_ppm = exposure(in_ppm, expo);
        int wrt_num = write_ppm(out, out_ppm);
        if (ferror(out) || wrt_num != out_ppm->cols * out_ppm->rows) {
            printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
            exit = 7;
        }
        free_image(out_ppm);
    }

    //a-blend
    else if (strcmp(argv[3], abl) == 0) {
        if (argc != 6) {
            printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
            fclose(out);
            free_image(in_ppm);
            return 5;
        }
        
        float alph = atof(argv[5]);

        if (check_fargv(argv[4]) || check_floatargv(argv[5]) || alph < 0 || alph > 1) {
            fclose(out);
            free_image(in_ppm);
            printf("Arguments for the specified operation were out of range for the given input image, or otherwise senseless\n");
            return 6;
        }

        FILE *in2 = fopen(argv[4], "rb");
        if (in2 == NULL) {
            printf("Specified input file could not be opened.\n");
            free_image(in_ppm);
            return 2;
        }
        if (check_in(in2)) {
            printf("Specified input file is not a properly-formatted PPM file, or reading input somehow fails\n");
            fclose(in);
            free_image(in_ppm);
            return 3;
        }
        Image *in_ppm2 = read_ppm(in2);
        
        fclose(in2);
        Image *out_ppm = ablend(in_ppm, in_ppm2, alph);
        int wrt_num = write_ppm(out, out_ppm);
        if (ferror(out) || wrt_num != out_ppm->cols * out_ppm->rows) {
            printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
            exit = 7;
        }
        free_image(in_ppm2);
        free_image(out_ppm);

    }
    
    // zoom in
    else if (strcmp(argv[3], z_in) == 0) {
        if (argc != 4) {
            printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
            fclose(out);
            free_image(in_ppm);
            return 5;
        }
        Image *out_ppm = zoom_in(in_ppm);
        int wrt_num = write_ppm(out, out_ppm);
        if (ferror(out) || wrt_num != out_ppm->cols * out_ppm->rows) {
            printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
            exit = 7;
        }
        free_image(out_ppm);
    }

    // zoom out
    else if (strcmp(argv[3], z_out) == 0) {
        if (argc != 4) {
            printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
            fclose(out);
            free_image(in_ppm);
            return 5;
        }
        Image *out_ppm = zoom_out(in_ppm);
        int wrt_num = write_ppm(out, out_ppm);
        if (ferror(out) || wrt_num != out_ppm->cols * out_ppm->rows) {
            printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
            exit = 7;
        }
        free_image(out_ppm);
    }

    // pointilism
    else if (strcmp(argv[3], ptl) ==0) {
        if (argc != 4) {
            printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
            fclose(out);
            free_image(in_ppm);
            return 5;
        }
        Image *out_ppm = pointilism(in_ppm);
        int wrt_num = write_ppm(out, out_ppm);
        if (ferror(out) || wrt_num != out_ppm->cols * out_ppm->rows) {
            printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
            exit = 7;
        }
        free_image(out_ppm);
    }

    // swirl
    else if (strcmp(argv[3], swr) == 0) {
        if (argc != 7) {
            printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
            fclose(out);
            free_image(in_ppm);
            return 5;
        }

        int cx = atoi(argv[4]);
        int cy = atoi(argv[5]);
        int strength = atoi(argv[6]);

        if (cx < 0 || cy < 0 || strength < 0 || check_intargv(argv[4]) || check_intargv(argv[5]) || check_intargv(argv[6]) || cx > in_ppm->cols || cy > in_ppm->rows) {
            fclose(out);
            free_image(in_ppm);
            printf("Arguments for the specified operation were out of range for the given input image, or otherwise senseless\n");
            return 6;
        }

        Image *out_ppm = swirl(in_ppm, cx, cy, strength);
        int wrt_num = write_ppm(out, out_ppm);
        if (ferror(out) || wrt_num != out_ppm->cols * out_ppm->rows) {
            printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
            exit = 7;
        }
        free_image(out_ppm);
    }

    // blur
    else if (strcmp(argv[3], bl) == 0) {
        if (argc != 5) {
            printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
            fclose(out);
            free_image(in_ppm);
            return 5;
        }
        float sigma = atof(argv[4]);
        if (check_floatargv(argv[4]) || sigma < 0) {
            fclose(out);
            free_image(in_ppm);
            printf("Arguments for the specified operation were out of range for the given input image, or otherwise senseless\n");
            return 6;
        }
        
        Image *out_ppm = blur(in_ppm, sigma);
        int wrt_num = write_ppm(out, out_ppm);
        if (ferror(out) || wrt_num != out_ppm->cols * out_ppm->rows) {
            printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
            exit = 7;
        }
        free_image(out_ppm);
    }

    else {
        printf("Operation specified is invalid.\n");
        return 4;
    }
    fclose(out);
    free_image(in_ppm);
    return exit;
}
